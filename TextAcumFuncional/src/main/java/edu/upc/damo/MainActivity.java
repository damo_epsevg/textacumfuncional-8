package edu.upc.damo;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    EditText camp;
    TextView res;
    Teclat teclat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        inicialitzaMenu(menu);
        return true;
    }

    private void inicialitza() {
        res =  findViewById(R.id.resultat);
        teclat = new Teclat(this);

    }

    private void esborraResultat() {
        res.setText("");
        camp.setText("");
    }

    private void inicialitzaMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_afegeix);
        programaAccions(menuItem);
        programaFocus(menuItem);

    }


    private void programaAccions(MenuItem menuItem) {
        camp =  (EditText) menuItem
                .getActionView()
                .findViewById(R.id.entradaDada);
        camp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return nouContingut(v, actionId, event);
            }
        });
    }

    private void programaFocus(MenuItem menuItem) {
        // Les dues retro-crides han de retornar true

        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.expandActionView();
                return true;
            }
        });

        menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                camp.requestFocus();
                teclat.mostraTeclat(getApplicationContext(), camp);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                teclat.amagaTeclat(getApplicationContext(), camp);
                return true;
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.action_esborra:
                esborraResultat();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void mostraAjut() {
    }


    private boolean nouContingut(TextView v, int actionId, KeyEvent event) {

        switch (teclat.accio(actionId, event)) {
            case Teclat.OK:
               afegeixAResultat(camp.getText());
               camp.setText("");
               return true;
        }

        return true;
    }




    private void afegeixAResultat(Editable text) {
        res.append("\n");
        res.append(text);
    }
}


