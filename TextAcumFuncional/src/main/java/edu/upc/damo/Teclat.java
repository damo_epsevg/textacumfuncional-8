package edu.upc.damo;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Josep M on 09/07/2015.
 */
public class Teclat {
    public final static int  DESCONEGUT = 0;
    public final static int  OK = 1;

    TeclatAbstracte teclat;

    public Teclat(Context context){
        teclat = getInstanceTeclat(context);
    }

    public void amagaTeclat(Context c,View v) {
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void mostraTeclat(Context c, View v) {
        amagaTeclat(c, v);    // Neccessari, ja que no sé per quins set sous, sense ell el primer cop no xuta
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.RESULT_SHOWN);
    }


    public boolean hiHaTeclatHard(Context c) {
        return c.getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
    }

    public TeclatAbstracte getInstanceTeclat(Context c){
        if (hiHaTeclatHard(c))
            return new TeclatHard();
        else
            return new TeclatSoft();
    }

    public int accio( int actionId, KeyEvent event) {
        return teclat.accio(actionId, event);
    }

    public abstract class TeclatAbstracte {
         public abstract int accio( int actionId, KeyEvent event);
    }

    private class TeclatHard extends TeclatAbstracte {
        @Override
        public int accio(int actionId, KeyEvent event) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_UP:
                    return OK;
            }
            return DESCONEGUT;
        }
    }

    private class TeclatSoft extends TeclatAbstracte {
        @Override
        public int accio(int actionId, KeyEvent event) {
            switch (actionId) {
                case EditorInfo.IME_ACTION_GO:
                    return OK;
            }
            return DESCONEGUT;
        }
    }
}